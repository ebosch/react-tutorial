import { render } from "react-dom";
import "stylesheets/stylesheet.css";

///
/// I'm a simple header component
///

class Header extends Component {
  render() {
    return (
      <div className="header" onClick={() => alert("Sup ya'll")}>
        Psst. I'm a header :)
      </div>
    );
  }
}

//
// This is a react component called Commment.
//

const Comment = ({ title }) => (
  <div className="comment">
    <h2>{title}</h2>
    <p>I should totally insert something here</p>
  </div>
);

///
// This is a react component called
// MyApplication. This is our root component
//

const MyApplication = () => (
  <div className="myapplication">
    <Header />
    <h1>Emile's 90's guestbook ya'll</h1>
    <Comment title="Omg i love react!" />
    <Comment title="This is craaazyyy" />
  </div>
);

//
// This below says: Render the component MyApplication
// to the element of which the id is app
// see in public/index.html:
// <div id='app'></div>
//
render(<MyApplication />, document.getElementById("app"));
