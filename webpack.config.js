var path = require("path");
var webpack = require("webpack");
var CopyWebpackPlugin = require("copy-webpack-plugin");
var HtmlWebpackPlugin = require("html-webpack-plugin");
var fs = require("fs");
var path = require("path");
var prod =
  process.argv.indexOf("-p") != -1 || process.env.NODE_ENV == "production";

module.exports = {
  entry: "./app/index.js",
  devtool: prod ? false : "inline-source-map",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: prod ? "assets/[name].[chunkhash].js" : "assets/[name].js"
  },
  resolve: {
    mainFiles: ["index"],
    modules: [path.resolve(__dirname, "app"), "node_modules"],
    extensions: [".js", ".css"]
  },
  plugins: [
    new CopyWebpackPlugin([{ from: "public", force: true }]),
    new HtmlWebpackPlugin({ template: "public/index.html" }),
    new webpack.ProvidePlugin({
      ReactDOM: "react-dom",
      React: "react",
      Component: ["react", "Component"]
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: "vendor",
      minChunks: function(module) {
        return module.context && module.context.indexOf("node_modules") !== -1;
      }
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: "meta",
      chunks: ["vendor"]
    })
  ],
  module: {
    loaders: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      { test: /\.(js)$/, exclude: /node_modules/, loader: "babel-loader" }
    ]
  }
};
