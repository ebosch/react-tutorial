# README

Sup y'all! React demo repo yeah

## Getting started

In order to get the sample app working clone it first:

```
git clone git@bitbucket.org:ebosch/react-tutorial.git
cd react-tutorial
npm install -g yarn
yarn
```

And then to run the dev server:

```
yarn run dev
```

Then in a new tab, you can open the generated index.html.

```
open dist/index.html 
```

## The tutorials

- Simpel react component
- React component in different files
- Stateless vs stateful components
- The need for keys when iterating